# msksMobileAppIOSPod

[![CI Status](https://img.shields.io/travis/Armin/msksMobileAppIOSPod.svg?style=flat)](https://travis-ci.org/Armin/msksMobileAppIOSPod)
[![Version](https://img.shields.io/cocoapods/v/msksMobileAppIOSPod.svg?style=flat)](https://cocoapods.org/pods/msksMobileAppIOSPod)
[![License](https://img.shields.io/cocoapods/l/msksMobileAppIOSPod.svg?style=flat)](https://cocoapods.org/pods/msksMobileAppIOSPod)
[![Platform](https://img.shields.io/cocoapods/p/msksMobileAppIOSPod.svg?style=flat)](https://cocoapods.org/pods/msksMobileAppIOSPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

msksMobileAppIOSPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'msksMobileAppIOSPod'
```

## Author

Armin, armin.mahmutovic@ngs.ba

## License

msksMobileAppIOSPod is available under the MIT license. See the LICENSE file for more info.
